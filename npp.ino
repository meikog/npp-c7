// HC-05 Bluetooth Transponder
// Das Modul mit AT Kommandos programmieren
//
// Matthias Busse 17.6.2014 Version 1.0

#include <SoftwareSerial.h>
#include <SimpleTimer.h>
#include "TimerOne.h"
//#include <digitalWriteFast.h>
SimpleTimer timer;

SoftwareSerial mySerial(19, 11); // RX, TX
int i=0;
String receivedchar;
int test = 2;

void setup() {
  Timer1.initialize(5000);
  Timer1.pwm(1, 200);
  pinMode(9, OUTPUT);
  mySerial.begin(9600);
  Serial.begin(9600);
  Serial1.begin(9600);
  Serial.println("Komandos eingeben");
  delay(500);
  digitalWrite(9, 1); // Programmier-Pin für KEY aktiv
}

void loop() {
  if (Serial1.available()){ // BT > PC
    receivedchar = Serial1.read();
    test = receivedchar.toInt();
    Serial.write(test);
    
    if(test == '1'){
      Timer1.pwm(1, 200);
      Serial.write("auf");
    }
    
    if(test == '2'){
      Timer1.pwm(1, 800);
      Serial.write("zu");
    }
    
  }
  
  if (Serial.available()) {  // PC > BT
   Serial.write(Serial.read());

   receivedchar = Serial.read();
    test = receivedchar.toInt();
    Serial.write(test);
    
    if(test == '1'){
      Timer1.pwm(1, 200);
      Serial.write("auf");
    }
    
    if(test == '2'){
      Timer1.pwm(1, 810);
      Serial.write("zu");
    }
  }
}